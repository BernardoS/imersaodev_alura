const num1Element = document.querySelector("#num1");
const num2Element = document.querySelector("#num2");
const resultadoElement = document.querySelector("#resultado");
const calcularElement = document.querySelector("#calcular")
const operacaoElement = document.querySelector("#operacao")

operacaoElement.addEventListener("click", mudaOp);

calcularElement.addEventListener("click", function () {
    resultadoElement.value = calcula(parseFloat(num1Element.value), parseFloat(num2Element.value));
}, false);


function mudaOp() {

    switch (document.getElementById("calcular").innerText) {

        case "+":
            document.getElementById("calcular").innerHTML = "<h2>-</h2>";
            break;
        case "-":
            document.getElementById("calcular").innerHTML = "<h2>*</h2>";
            break;
        case "*":
            document.getElementById("calcular").innerHTML = "<h2>/</h2>";
            break;
        case "/":
            document.getElementById("calcular").innerHTML = "<h2>+</h2>";
            break;
    }


}


function calcula(x, y) {

if(num1Element.value == "" || num2Element.value == ""){
    return "Digite os valores para calcular!"
}

    switch (document.getElementById("calcular").innerText) {

        case "+":
            return x + y;
        case "-":
            return x - y;
        case "*":
            return (x * y).toFixed(2);
        case "/":
            return (x / y).toFixed(2);
    }


}

