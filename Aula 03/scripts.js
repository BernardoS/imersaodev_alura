const numeroElement = document.querySelector("#numero");
const chuteElement = document.querySelector("#chutar");
const messagemElement = document.querySelector("#messagem")
var chances = 5, numeroSecreto = geraNumero(), fimDeJogo = false;

chuteElement.addEventListener("click", function () {
    chute(parseInt(numero.value));
}, false);

function refresh() {
    setTimeout(function () {
        window.location.reload(1);
    }, 5000); // 5 seg
}

function geraNumero() {
    chances = 5;
    return Math.floor(Math.random() * 50) + 1;


}

function acertou() {
    fimDeJogo = true;
    messagemElement.style.color = "green";
    messagemElement.innerHTML = 'Parabéns você acertou com ' + (chances) + ' tentativas(s) restante(s)' + '<br>' + ' A página será atualizada em 5 segundos';
    refresh();
}

function perdeu() {
    messagemElement.innerHTML = 'Você perdeu, suas chances acabaram! ' + 'O número secreto era: ' + numeroSecreto + '<br>' + ' A página será atualizada em 5 segundos';
    chuteElement.value = 'Resetar';
    refresh();

}

function alteraChances() {
    return 'Chutar (' + chances + ')';
}

function chute() {

    if (chances == 0 || fimDeJogo == true) {
        messagemElement.innerHTML = 'Resetando o jogo';
        refresh()

    } else {

        if (numeroElement.value != '' && numeroElement.value <= 50 && numeroElement.value > 0) {
            chances--;
            chuteElement.innerHTML = alteraChances();
          
            if (chances == 0) {
               perdeu();
            } else {

               
            

                if (numeroElement.value == numeroSecreto) {
                    acertou();
                } else if (numeroElement.value < numeroSecreto) {
                    messagemElement.innerText = 'Você errou, tente um número maior';

                } else {
                    messagemElement.innerText = 'Você errou, tente um número menor';
                }

            }


        } else {
            messagemElement.innerText = 'Digite um valor válido';
        }
    }
}


